const getSum = (str1, str2) => {
  if (typeof str2 !== "string" || typeof str1 !== "string" || str1.match(/\D/) || str2.match(/\D/)) {
      return false;
  }
  let s1 = str1.split("").reverse();
  let s2 = str2.split("").reverse();
  let result = [];
  for (let i = 0; i < (s1.length >= s2.length ? s1.length : s2.length); i++) {
      if (s1[i] == undefined) { s1[i] = 0; }
      else if (s2[i] == undefined){
         s2[i] = 0;}
      if (+s1[i] + +s2[i] > 10) {
          result.push(s1[i] + s2[i] - 10);
          s1[i + 1] += 1;
      } else {
          result.push(+s1[i] + +s2[i]);
      }
  }
  return result.reverse().join("");
}


const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let posts = 0;
  let comments = 0;
  for (const post of listOfPosts) {
      if (post.author === authorName){ posts++ }
      if (post.comments !== undefined) {
          for (const comment of post.comments) {
          if (comment.author === authorName) { comments++ }
      }
      }
  }
  return "Post:" + posts +",comments:" + comments;
}


const tickets=(people)=> {
  let money = 0;
  const profit = 25;
  for(let i = 0; i < people.length; i++)
  {
    let change = 0;
    switch(people[i]) {
      case 35: 
        change = 10;
        break;

      case 50:  
        change = 25;
        break;

      case 100: 
      change = 75;
        break;

        default:
        break;
}

    money -= change;
    if(money < 0){
        return "NO";
    }
    money += profit;
  }
  return "YES";
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
